import argparse
import socket
import struct
import time
import crc16
from cobs import cobs

sock = None

COMMAND_WIREMAP = 48
RESULT_WIREMAP = 4144
COMMAND_DEVICEINFO = 1
RESULT_DEVICEINFO = 4097

COMMAND_LINK = 52
COMMAND_DHCP = 56

lastData = {}


def connect(pockethernet_mac):
    global sock

    # Pockethernet seems to use RFCOMM channel 5
    while True:
        try:
            sock = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
            sock.bind((socket.BDADDR_ANY, 0))
            sock.connect((pockethernet_mac, 5))
            print("Connected!")
            break
        except OSError as e:
            print(e)
            time.sleep(1)


def send_command(command, data):
    global sock
    print("Sending command {} with {} bytes data".format(command, len(data)))

    # First is command ID, the 0 is reserved space for a checksum
    header = struct.pack('<HH', command, 0)
    data = struct.pack('<{}s'.format(len(data)), data)
    packet = header + data

    crc = crc16.crc16xmodem(packet, -1)
    header = struct.pack('<HH', command, crc)
    packet = header + data

    encoded = cobs.encode(packet)
    raw = b'\0' + encoded + b'\0'
    sock.send(raw)


def read_packet(until=None):
    global sock
    global lastData
    while True:
        raw = sock.recv(200)
        packets = raw.split(b'\0')
        packets = list(filter(None, packets))
        for packet in packets:
            packet = cobs.decode(packet)
            header = struct.unpack_from('<HH', packet)
            data = struct.unpack_from('<{}s'.format(len(packet) - 4), packet, 4)
            packet_type = header[0]
            lastData[packet_type] = data[0]

            if packet_type == until:
                return data[0]


def get_device_info():
    send_command(COMMAND_DEVICEINFO, b'')
    result = read_packet(RESULT_DEVICEINFO)
    result = struct.unpack_from('<BBBBBB', result)
    mac = '28:FD:80:{:02X}:{:02X}:{:02X}'.format(result[2], result[1], result[0])
    return {
        'hardware': result[4],
        'firmware': result[5],
        'mac': mac
    }


def get_wiremap():
    send_command(COMMAND_WIREMAP, b'')
    result = read_packet(RESULT_WIREMAP)
    print(result)

    connections = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    shorts = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(0, 4):
        a = (i * 2) + 1
        connections[a] = result[i] & 15
        shorts[a] = result[i + 4] & 15
        b = (i * 2) + 2
        connections[b] = (result[i] & 240) >> 4
        shorts[b] = (result[i + 4] & 240) >> 4

    connections[0] = result[8] & 15
    shorts[0] = (result[8] & 240) >> 4

    return {
        'connections': connections,
        'shorts': shorts,
        'id': result[9]
    }


def get_link_info():
    send_command(COMMAND_DHCP_EN, b'')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Pockethernet protocol test implementation")
    parser.add_argument('pockethernet', help='Pockethernet MAC')
    parser.add_argument('bytes', type=int)
    args = parser.parse_args()

    connect(args.pockethernet)
    print(get_device_info())
    print(get_wiremap())
